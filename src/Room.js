class Room {
  constructor(name) {
    this.name = name
    this.clients = new Map();
    this.count = 0;
  }

  Join(userId, socket) {
    this.clients.set(userId, socket);
    this.count++;
    return socket.id;
  }

  Leave(_index) {
    this.clients.delete(_index);
    this.count--;
    console.log(this.clients.keys());
  }

  BroadcastAll(msg) {
    this.clients.forEach((client, key, map) => {
      client.write(msg);
      console.log(`send message - ${key}`);
  });
  }

  BroadcastEx(sender, msg) {
    this.clients.forEach((client, key, map) => {
      if (client.id != sender)
        client.write(msg);
    });
  }

  SendTo(_index, msg) {
    this.clients.get(_index).write(msg);
  }
}

module.exports = Room;