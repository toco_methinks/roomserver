const http = require('http').createServer();
const sockjs = require('sockjs');
const rm = require('./RoomManager');
const RoomManager = new rm();


const sockServer = sockjs.createServer({disconnect_delay : 1000});

sockServer.on('connection', function (con) {
  console.log("new client");

  con.on('data', message => {
    con.roomIndex = 1;
    RoomManager.handleMessage(con, message);
  });

  con.on('close', function () {
    console.log('close ' + con.userId);
    // console.log('close ' + con.roomIndex);
    RoomManager.leaveRoom(con);
  });
});


http.listen(3000, '0.0.0.0');
sockServer.installHandlers(http);

