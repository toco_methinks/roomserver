const Room = require('./Room');

class RoomManager {
  constructor() {
    this.rooms = new Map();
  }

  createRoom(name) {
    const room = this.rooms.get(name);
    if (!room){
      this.rooms.set(name, new Room(name));
      console.log('create Room');
    }
  }

  GetRoom(name) {
    this.createRoom(name);
    return this.rooms.get(name);
  }

  handleMessage(con, message) {
    if (message == undefined)
      return;

    const data = JSON.parse(message);

    // if (data.type == 'createSession') {
    //   // TODO : check user etc...
    //   this.createRoom(data.message.sessionId);
    //   return;
    // }

    let room = this.GetRoom(data.msg.sessionId);


    if (!room)
      return;
    console.log(data.type);
    try{
      switch (data.type) {
        case 'joinSession':
          con.methinksRoomIndex = room.name;
          con.userId = data.msg.fromUserId;
          console.log(`${con.userId} join in room`);
          data.msg.type = 'added';
          room.BroadcastAll(JSON.stringify({ ...data }));
          room.Join(data.msg.fromUserId, con);
          break;
        case 'offer':
          room.SendTo(data.msg.toUserId, JSON.stringify({ ...data }));
          break;
        case 'answer':
          room.SendTo(data.msg.toUserId, JSON.stringify({ ...data }));
          break;
        case 'candidate':
          console.log(`on candiate to - ${data.msg.toUserId}`);
          room.SendTo(data.msg.toUserId, JSON.stringify({ ...data }));
          break;
        case 'sendMessage':
          if(!data.msg.toUserId)
            room.BroadcastEx(con.id, JSON.stringify({ ...data }));
          else
            room.SendTo(data.msg.toUserId, JSON.stringify({ ...data }));
          break;
      }
    }

    catch(e){
      console.log("error occur");
      console.log(data.msg);
      // console.log(e);
    }
  }

  leaveRoom(con) {
    let room = this.GetRoom(con.methinksRoomIndex);
    console.log(room.name);
    if (room) {
      room.Leave(con.userId);
    }
  }
}

module.exports = RoomManager;