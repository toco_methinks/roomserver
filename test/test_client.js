
let sock;
let gUserId;
let localStream;
let localVideo;

let myUserType = "normal";

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

document.addEventListener('DOMContentLoaded', function () {
  localVideo = document.getElementById('localVideo');
  remoteVideo = document.getElementById('remoteVideo');

  navigator.mediaDevices.getUserMedia({
      video: true,
      audio: true
  }).then(stream => {
    if(myUserType != "observer")
      localVideo.srcObject = stream;
    localStream = stream;
    // let id =  Math.random();
    let id = "yan" + getRandomInt(20);
    console.log(id);
    init(id);
  });
})

let time1;
let time2;

function init(userid) {
  sock = new WebSocket('ws://localhost:3000/websocket');
    // sock = new WebSocket('wss://rtc-dev.methinks.io/websocket');
  sock.onopen = OnOpen;
  sock.onmessage = OnMessage;
  sock.onclose = OnClose;
  gUserId = userid;
};

function send(type, data) {
  sock.send(JSON.stringify({ type: type, msg: { ...data, sessionId: "1234", fromUserId: gUserId} }));
}

function OnOpen() {
  time1 = new Date();
  send("joinSession", {userType: myUserType});
}

var peerConnections = {};
const remoteVideos = {};

function getPeerConnection(userId) {
    return peerConnections[userId];
}


function OnMessage(e) {
  data = JSON.parse(e.data);
  msg = data.msg;
  switch (data.type) {
    case "joinSession":
      console.log("new client join!!");
      createPeerConnection(msg.fromUserId);
      console.log("send offer");
      offer(msg.fromUserId);
      break;

    case "offer":
      console.log("receive offer");
      createPeerConnection(msg.fromUserId);
      console.log("send answer");
      answer(msg.fromUserId, msg.sdpData);
      break;

    case "answer":
      console.log("receive answer");
      getPeerConnection(msg.fromUserId).setRemoteDescription(msg.sdpData);
      // send("sendMessage", { sessionId: "1234", data: "test", toUserId: msg.fromUserId });
      break;

    case "candidate":
      console.log(msg.fromUserId);
      // console.log(msg.candidateData);
      getPeerConnection(msg.fromUserId).addIceCandidate(msg.candidateData);
      // console.log(data);
      break;

    case "sendMessage":
      console.log("receive message");
      break;
  }
}

function OnClose() {
  time2 = new Date();
  console.log(time2 - time1);
  console.log('close');
}

function createPeerConnection(userId) {
  let peerConnection = new RTCPeerConnection({
      'iceServers': [
          {
              'urls': ['stun:stun.l.google.com:19302']
          },
          {
              'urls': ['turn:w1.xirsys.com:80?transport=udp'],
              'username': '35f846ee-be26-11e8-a7fe-35e8f7ade332',
              'credential': '35f84770-be26-11e8-b1ea-1c6f81131643'
          }
      ]
  });
  const hTag = document.createElement('h1');
  hTag.innerText = userId;
  document.body.append(hTag);
  const tempVideoTag = document.createElement("video");
  tempVideoTag.setAttribute('autoplay', "");
  document.body.append(tempVideoTag);
  remoteVideos[userId] = tempVideoTag;

  peerConnections[userId] = peerConnection;
  if(myUserType != "observer"){
    console.log(myUserType);
    localStream.getTracks().forEach(track => {peerConnection.addTrack(track, localStream); //console.log(track)
    });
  }
  peerConnection.onicecandidate = (evt) => onIceCandidate(userId, evt);
  peerConnection.ontrack = (evt) => onGetRemoteMedia(userId, evt);
}

function onIceCandidate(userId, evt) {
  if(!evt.candidate)
    return;
  send("candidate", {toUserId: userId, candidateData: evt.candidate});
}

function offer(userId) {
  let peerConnection = getPeerConnection(userId);
  peerConnection.createOffer()
      .then(function (sdp) {
          peerConnection.setLocalDescription(sdp).then(function () {
              send("offer", {userType: myUserType, toUserId: userId, sdpData: sdp});
          });
      })
      .catch(function (err) {
          console.log(err);
      });
}

function answer(userId, offerSdp) {
  let peerConnection = getPeerConnection(userId);
  peerConnection.setRemoteDescription(offerSdp).then(function () {
      peerConnection.createAnswer().then(function (sdp) {
          peerConnection.setLocalDescription(sdp).then(function () {
              send("answer", { sessionId: "1234", sdpData: sdp, toUserId: userId});
          });
      });
  });
}

function onGetRemoteMedia(userId, evt) {
  remoteVideos[userId].srcObject = evt.streams[0];
}